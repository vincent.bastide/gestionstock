'use strict'

const controller = require('../controller/userController'),
	express = require('express'),
	router = express.Router(),
	{ check } = require('express-validator')

router.post('/login', [ check('email').isEmail(), check('password').isLength({ min: 4 }) ], controller.checkLogin)

router.post('/logout', controller.logout)

router.get('/createAccount', (req, res) => res.render('create_account', { title: 'Créer utilisateur' }))
router.post('/createAccount', controller.createUser)

module.exports = router
