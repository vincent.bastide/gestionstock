const controller = require('./../controller/itemController'),
    check = require("../middlewares/checkController"),
    express = require('express'),
    router = express.Router(),
    { redirectLogin } = require('./../middlewares/login')

router.get('/createItem/', redirectLogin, controller.createItem)
router.post('/createItem/', check.validateItem("createItem") , controller.insertItem)

router.get('/edit/:id/', redirectLogin, controller.editItem)
router.post('/edit/:id/',check.validateItem("editItem"), controller.updateItem)

router.post('/delete/:id/', controller.deleteItem)

module.exports = router