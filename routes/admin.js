'use strict'

const controller = require('../controller/adminController'),
	express = require('express'),
	router = express.Router(),
	 check  = require("../middlewares/checkController"),
	{ redirectLogin } = require('./../middlewares/login'),
	{ redirectAdmin } = require('./../middlewares/admin')

//Route principale
router.get('/', [ redirectLogin, redirectAdmin], controller.adminHome)

// Route création utilisateur
router.get('/create', [ redirectLogin, redirectAdmin], controller.createUser)
router.get('/create/:errors', [ redirectLogin, redirectAdmin], controller.createUser)
router.post('/create', check.validateUser('createUser'),controller.addUser)

// Route edition utilisateur
router.get('/edit/:id/', [ redirectLogin, redirectAdmin], controller.editUser)
router.post('/edit/:id/',check.validateUser('editUser'), controller.updateUser)

// Route deletion utilisateur
router.post('/delete/:id/',controller.deleteUser)


module.exports = router
