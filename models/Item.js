class Item {
	// Constructor
	constructor(ref, name, quantity, price, section, role) {
		this.ref = ref
		this.name = name
		this.quantity = quantity
		this.price = price
		this.section = section
		this.role = role
	}

	// Getter
	get ref() {
		return this.ref
	}

	get name() {
		return this.name
	}

	get quantity() {
		return this.quantity
	}

	get price() {
		return this.price
	}

	get section() {
		return this.section
	}

	get role() {
		return this.role
	}

	// Setter
	set ref(ref) {
		this.ref = ref
	}

	set name(name) {
		this.name = name
	}

	set quantity(quantity) {
		this.quantity = quantity
	}

	set price(price) {
		this.price = price
	}

	set section(section) {
		this.section = section
	}

	set role(role) {
		this.role = role
	}
}
