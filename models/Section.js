class Section {
	//Constructor
	constructeur(id, name) {
		this.id = id
		this.name = name
	}

	// Getter
	get id() {
		return this.id
	}

	get name() {
		return this.name
	}

	// Setter
	set id(id) {
		this.id = id
	}

	set name(name) {
		this.name = name
	}
}
