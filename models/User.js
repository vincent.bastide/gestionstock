'use strict'

class User {
	constructor(id, firstname, lastname, email, password) {
		this.id = id
		this.firstname = firstname
		this.lastname = lastname
		this.email = email
		this.password = password
	}

	// Getters
	get id() {
		return this.id
	}

	get firstname() {
		return this.firstname
	}

	get lastname() {
		return this.lastname
	}

	get email() {
		return this.email
	}

	get password() {
		return this.password
	}

	// Setters
	set id(id) {
		this.id = id
	}

	set firstname(firstname) {
		this.firstname = firstname
	}

	set lastname(lastname) {
		this.lastname = lastname
	}

	set email(email) {
		this.email = email
	}

	set password(password) {
		this.password = password
	}
}
