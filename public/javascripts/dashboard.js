'use strict'

// DOM Vars
const $selectedShop = document.getElementById('store'),
    $selectedSection = document.getElementById('section')

let $elementsToHide = undefined

// Vars
let shopIndex = $selectedShop.options[$selectedShop.selectedIndex].value,
    sectionIndex = $selectedSection.options[$selectedSection.selectedIndex].value

const showSelectedRows = _ => {
    const $elementsToShow = sectionIndex === 'all' ?
        document.getElementsByClassName(`shop_${shopIndex}`) :
        document.getElementsByClassName(`shop_${shopIndex} section_${sectionIndex}`)

    // Hide previous elements
    if ($elementsToHide !== undefined) {
        for (const $row of $elementsToHide) {
            $row.style.display = 'none'
        }
    }

    // show wanted elements
    for(const $row of $elementsToShow) {
        $row.style.display = 'table-row'
    }
    $elementsToHide = $elementsToShow
}

$selectedShop.addEventListener('change', _ => {
    shopIndex = $selectedShop.options[$selectedShop.selectedIndex].value    
    showSelectedRows()
})

$selectedSection.addEventListener('change', _ => {
    sectionIndex = $selectedSection.options[$selectedSection.selectedIndex].value
    showSelectedRows()
})

showSelectedRows()