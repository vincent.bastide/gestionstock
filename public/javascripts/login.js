'use strict'

const $cover = document.getElementById('cover')

// Listeners
const loadingListener = () => $cover.classList.add('fadeOut')
const fadoutListener = () => $cover.style.display = 'none'

// Events
window.addEventListener('load', loadingListener)
$cover.addEventListener('animationend', fadoutListener, false)