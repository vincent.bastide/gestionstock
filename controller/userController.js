'use strict'

const UserDAO = require('./DAO/UserDAO'),
	{ validationResult } = require('express-validator')

exports.checkLogin = (req, res) => {
	const errors = validationResult(req),
		user_DAO = new UserDAO()

	if (!errors.isEmpty()) return res.redirect(`/?errors=${encodeURIComponent(errors.array())}`)

	user_DAO.find(req.body.email).then((User) => {
		if (User === undefined) return res.redirect('/?error=ID')
		else {
			if (req.body.password === User.password) {
				req.session.user = User
				res.redirect('/dashboard')
			} else {
				res.redirect('/?errors=ID')
			}
		}
	})
}

exports.logout = (req, res) => {
	req.session.destroy((err) => {
		if (err) {
			return res.redirect('/dashboard')
		}
		res.clearCookie('session')
		res.redirect('/')
	})
}

exports.createUser = (req, res) => {
	console.log(req.query)
}
