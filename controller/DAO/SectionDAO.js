const DAO = require('./DAO')
const pool = require('../../models/Database')

class SectionDAO extends DAO {
	find(id) {
		const SQL_QUERY = 'SELECT * FROM RAYON WHERE id=?'

		return new Promise((resolve) => {
			pool.getConnection((err, connection) => {
				if (err) throw err
				connection.query(SQL_QUERY, [ id ], (errors, results) => {
					if (errors) throw errors
					resolve(results[0])
					connection.release()
				})
			})
		})
	}

	getSectionBy(nom) {
		const SQL_QUERY = `SELECT id FROM RAYON WHERE nom='${nom}'`

		return new Promise((resolve) => {
			pool.getConnection((err, connection) => {
				if (err) throw err
				connection.query(SQL_QUERY, (errors, results) => {
					if (errors) throw errors
					resolve(results)
					connection.release()
				})
			})
		})
	}
	create() {}

	update() {}

	delete() {}

	getAll() {
		const SQL_QUERY = 'SELECT * FROM RAYON'

		return new Promise((resolve) => {
			pool.getConnection((err, connection) => {
				if (err) throw err
				connection.query(SQL_QUERY, (errors, results) => {
					if (errors) throw errors
					resolve(results)
					connection.release()
				})
			})
		})
	}
}

module.exports = SectionDAO
