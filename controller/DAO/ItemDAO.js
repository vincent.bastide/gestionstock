const mysql = require('mysql'),
	pool = require('../../models/Database'),
	DAO = require('./DAO')


class ItemDAO extends DAO {
	find(ref) {
		const SQL_QUERY = 'SELECT * FROM ARTICLE WHERE reference=?'

		return new Promise((resolve) => {
			pool.getConnection((err, connection) => {
				if (err) throw err
				connection.query(SQL_QUERY, [ ref ], (errors, results) => {
					if (errors) throw errors
					resolve(results[0])
					connection.release()
				})
			})
		})
	}

	create(ref, name, quantity, price, section, shop) {
		const SQL_QUERY = 'INSERT INTO ARTICLE VALUES (NULL, ?, ?, ?, ?, ?, ?)'

		pool.getConnection((err, connection) => {
			if (err) throw err
			connection.query(SQL_QUERY, [ ref, name, quantity, price, section, shop ], (errors, result) => {
				if (errors) throw errors
				connection.release()
			})
		})
	}

	update(ref, name, quantity, price, section, shop) {
		const SQL_QUERY = 'UPDATE ARTICLE SET nom=?, qte=?, prix=?, id_rayon=?, id_magasin=? WHERE reference=?'

		pool.getConnection((err, connection) => {
			if (err) throw err
			connection.query(SQL_QUERY, [ name, quantity, price, section, shop, ref ], (errors, result) => {
				if (errors) throw errors
				connection.release()
			})
		})
	}

	delete(ref) {
		const SQL_QUERY = 'DELETE FROM ARTICLE WHERE reference=?'

		pool.getConnection((err, connection) => {
			if (err) throw err
			connection.query(SQL_QUERY, [ ref ], (errors, result) => {
				if (errors) throw errors
				connection.release()
			})
		})
	}

	getAll() {
		const SQL_QUERY = 'SELECT * FROM ARTICLE'

		return new Promise((resolve) => {
			pool.getConnection((err, connection) => {
				if (err) throw err
				connection.query(SQL_QUERY, (errors, results) => {
					if (errors) throw errors
					resolve(results)
					connection.release()
				})
			})
		})
	}
}

module.exports = ItemDAO
