const mysql = require('mysql'),
	pool = require('../../models/Database'),
	DAO = require('./DAO')

class userDAO extends DAO {
	constructor() {
		super()
	}

	find(email) {
		const SQL_QUERY = 'SELECT * FROM USERS WHERE email=?'

		return new Promise((resolve) => {
			pool.getConnection((err, connection) => {
				if (err) throw err
				connection.query(SQL_QUERY, [ email ], (errors, results) => {
					if (errors) throw errors
					resolve(results[0])
					connection.release()
				})
			})
		})
	}

	create(name, firstname, email, password, role, section) {
		const SQL_QUERY = 'INSERT INTO USERS VALUES (NULL,  ?, ?, ?,?, ?, ?)'
		
		if(section == "NULL") {
			section = null
		}

		pool.getConnection((err, connection) => {
			if (err) throw err
			connection.query(SQL_QUERY, [  name, firstname, email, password, role, section ], (errors, result) => {
				if (errors) throw errors
				connection.release()
			})
		})
	}

	update(id, name, firstname, email, role, section) {
		const SQL_QUERY = 'UPDATE USERS SET nom=?, prenom=?, email=?, id_role=?, id_rayon=? WHERE id=?'
		
		if(section =="NULL") {
			section = null
		}
	
		pool.getConnection((err, connection) => {
			if (err) throw err
			connection.query(SQL_QUERY, [ name, firstname, email, role, section, id ], (errors, result) => {
				if (errors) throw errors
				connection.release()
			})
		})
	}

	delete(id) {
		const SQL_QUERY = 'DELETE FROM USERS WHERE id=?'

		pool.getConnection((err, connection) => {
			if (err) throw err
			connection.query(SQL_QUERY, [ id ], (errors, result) => {
				if (errors) throw errors
				connection.release()
			})
		})
	}
	
	getAll() {
		const SQL_QUERY = 'SELECT * FROM USERS'
		let users = undefined

		return new Promise((resolve) => {
			pool.getConnection((err, connection) => {
				if (err) throw err
				connection.query(SQL_QUERY, (errors, results) => {
					if (errors) throw errors
					resolve(results)
					connection.release()
				})
			})
		})
	}
}

module.exports = userDAO
