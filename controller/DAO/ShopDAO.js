const pool = require('../../models/Database'),
	DAO = require('./DAO')

class ShopDAO extends DAO {
	find(id) {
		const SQL_QUERY = 'SELECT * FROM MAGASIN WHERE id=?;'

		return new Promise((resolve) => {
			pool.getConnection((err, connection) => {
				if (err) throw err
				connection.query(SQL_QUERY, [ id ], (errors, results) => {
					if (errors) throw errors
					resolve(results[0])
					connection.release()
				})
			})
		})
	}

	create() {}

	update() {}

	delete() {}

	getAll() {
		const SQL_QUERY = 'SELECT * FROM MAGASIN'

		return new Promise((resolve) => {
			pool.getConnection((err, connection) => {
				if (err) throw err
				connection.query(SQL_QUERY, (errors, results) => {
					if (errors) throw errors
					resolve(results)
					connection.release()
				})
			})
		})
	}
}

module.exports = ShopDAO
