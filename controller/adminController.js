'use strict'
const UserDAO = require('./DAO/UserDAO')
const RoleDAO = require('./DAO/RoleDAO'),
    SectionDAO = require('./DAO/SectionDAO'),
    { validationResult } = require('express-validator')

// Fonction qui renvoit la liste des roles et des rayons depuis la BDD
const getRolesAndSections = async _ => {
    let roleDAO = new RoleDAO(),
        sectionDAO = new SectionDAO(),
        rolesArray = [],
        sectionArray = []

    await roleDAO.getAll().then(roles => {
        roles.forEach(element => rolesArray.push(element))
    })

    await sectionDAO.getAll().then(sections =>{
        sections.forEach(element => sectionArray.push(element))
    })
    return { rolesList: rolesArray, sectionsList: sectionArray }
}
// Affichage du dashboard utilisateur
exports.adminHome = async (req, res) => {
    let options = {
		title: 'Utilisateurs',
        admin: req.session.user.id_role > 1 ? true : false,
        root: req.session.user.id_role === 3 ? true : false,
    }
    
    let userDAO = new UserDAO(),
        userArray = []

    await userDAO.getAll().then((users) =>{
        users.forEach(element => userArray.push(element))
    })

    options.users = userArray
    res.render('./admin/admin', options)
}
// Affichage de la page pour créer un utilisateur
exports.createUser =  async (req, res) => {
    let options = {
        title: 'Ajouter utilisateur',
        admin: req.session.user.id_role > 1 ? true : false,
        errors: req.params.errors ? JSON.parse(decodeURIComponent(req.params.errors)) : {}
    }

    await getRolesAndSections().then(({rolesList,sectionsList}) => {
        options.roleDAO = rolesList
        options.sectionDAO = sectionsList
    })

    res.render('./admin/create_user',options)
}

// Ajout de l'utilisateur : insertion en BDD + rendu des erreurs 
exports.addUser =  (req,res) => {
    const userDAO = new UserDAO(),
        errors = validationResult(req),
        url = `/admin/create/${encodeURIComponent(JSON.stringify(errors.mapped()))}`
     
	if (!errors.isEmpty()) {
        res.redirect(url)
	} else {
		userDAO.create(
            req.body.name,
            req.body.firstname,
            req.body.email,
            req.body.password,
            req.body.selectRole,
            req.body.selectSection
        )
		res.redirect('/admin')
    }
}

// Affichage de la page de l'édition de l'utilisateur
exports.editUser = async (req,res) => {
    let options = {
        title: 'Editer utilisateur',
        admin: req.session.user.id_role > 1 ? true : false,
        sectionChief: req.session.user.id_rayon !== null ? true : false, 
        id: req.params.id,
        name: req.query.name ? req.query.name : undefined,
        firstname: req.query.firstname ? req.query.firstname : undefined,
        email: req.query.email ? req.query.email : undefined,
        role: req.query.role ? req.query.role : undefined,
        section: req.query.section ? req.query.section : undefined,
        errors: req.query.errors ? JSON.parse(decodeURIComponent(req.query.errors)) : {},
    }
 
    await getRolesAndSections().then(({ rolesList, sectionsList }) => {
        options.roleDAO = rolesList
        options.sectionDAO = sectionsList
        
    })
    res.render('./admin/edit_user',options)
    
}

// Mise à jour de l'utilisateur et insertion en BDD
exports.updateUser = (req,res) => {
    const userDAO = new UserDAO(),
        errors = validationResult(req),
        parameters = req.originalUrl.substring(req.originalUrl.indexOf('?'), req.originalUrl.length),
        url = `/admin/edit/${req.params.id}/${parameters}&errors=${encodeURIComponent(JSON.stringify(errors.mapped()))}`

	if (!errors.isEmpty()) {
        res.redirect(url)
	} else {
		userDAO.update(req.params.id, req.body.name, req.body.firstname, req.body.email, req.body.selectRole, req.body.selectSection)
		res.redirect('/admin')
    }
}

// Suppression de l'utilisateur
exports.deleteUser = (req, res) => {
    const userDAO = new UserDAO()
    userDAO.delete(req.params.id)
    res.redirect('/admin')
}