'use strict'

const ShopDAO = require('./DAO/ShopDAO'),
    SectionDAO = require('./DAO/SectionDAO'),
    ItemDAO = require('./DAO/ItemDAO'),
    { validationResult } = require('express-validator')


const getSectionsAndShops = async _ => {
    let shopDAO = new ShopDAO(),
        sectionDAO = new SectionDAO(),
        shopsArray = [],
        sectionArray = []

    await shopDAO.getAll().then(shops => {
        shops.forEach(element => shopsArray.push(element))
    })

    await sectionDAO.getAll().then(sections =>{
        sections.forEach(element => sectionArray.push(element))
    })
    return { shopsList: shopsArray, sectionsList: sectionArray }
}

exports.createItem = (req, res) => {
    let options = {
        title: 'Ajouter article', 
        admin: req.session.user.id_role > 1 ? true : false,
        sectionChief: req.session.user.id_rayon !== null ? true : false,
        errors: req.query.errors ? JSON.parse(decodeURIComponent(req.query.errors)) : {}
    }

    getSectionsAndShops().then(({ shopsList, sectionsList }) => {
        options.shopDAO = shopsList
        options.sectionDAO = sectionsList

        res.render('./items/create_item', options)
    })
}

exports.insertItem = (req, res) => {
	const itemDAO = new ItemDAO()
	const errors = validationResult(req)

	if (!errors.isEmpty()) {
		res.redirect(`/item/createItem/?errors=${encodeURIComponent(JSON.stringify(errors.mapped()))}`)
	} else {
        itemDAO.create(req.body.ref, req.body.name, req.body.quantity, req.body.price, req.body.selectSection, req.body.selectShop)
        res.redirect('/dashboard')
	}
}

exports.editItem = (req, res) => {
    let options = {
        title: 'Editer article',
        admin: req.session.user.id_role > 1 ? true : false,
        sectionChief: req.session.user.id_rayon !== null ? true : false,
        ref: req.params.id,
        name: req.query.name ? req.query.name : undefined,
        price: req.query.price ? req.query.price : undefined,
        quantity: req.query.quantity ? req.query.quantity : undefined,
        section: req.query.section ? req.query.section : undefined,
        shop: req.query.shop ? req.query.shop : undefined,
        errors: req.query.errors ? JSON.parse(decodeURIComponent(req.query.errors)) : {}
    }

    getSectionsAndShops().then(({ shopsList, sectionsList }) => {
        options.shopDAO = shopsList
        options.sectionDAO = sectionsList

        res.render('./items/edit_item', options)
    })
}

exports.updateItem = (req, res) => {
    const itemDAO = new ItemDAO(),
        errors = validationResult(req),
        parameters = req.originalUrl.substring(req.originalUrl.indexOf('?'), req.originalUrl.length),
        url = `/item/edit/${req.params.id}/${parameters}&errors=${encodeURIComponent(JSON.stringify(errors.mapped()))}`

    console.log(errors.mapped())
	if (!errors.isEmpty()) {
        res.redirect(url)
	} else {
		itemDAO.update(req.params.id, req.body.name, req.body.quantity, req.body.price, req.body.selectSection, req.body.selectShop)
		res.redirect('/dashboard')
	}
}

exports.deleteItem = (req, res) => {
    const itemDAO = new ItemDAO()
    itemDAO.delete(req.params.id)
    res.redirect('/dashboard')
}
