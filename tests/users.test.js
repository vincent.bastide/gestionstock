const request = require('supertest')
const session = require('supertest-session')
const app = require('../app')
const UserDAO = require('../controller/DAO/UserDAO')

describe('Sign in / Sign out', () => {
    it('Should sign in user', done => {
        request(app)
            .post("/users/login")
            .send({
                email: 'admin@admin.com',
                password: 'Adminapp'
            })
            .expect(302)
            .expect('Location', '/dashboard')
		done()
    })

    it('Should sign out user', done => {
        request(app)
            .post("/users/logout")
            .expect(302)
            .expect('Location', '/')
        done()
    })
})

describe('Users', () => {
    let authentificatedSession
    let testSession = null
    
    beforeEach(() => {
        testSession = session(app)
    })

    beforeEach(done => {
        testSession
            .post("/users/login")
            .send({
                email: 'admin@admin.com',
                password: 'Adminapp'
            })
            .expect(302)
            .expect('Location', '/dashboard')
            .end((err) => {
                if (err) return done(err)
                authentificatedSession = testSession
                return done()
            })
    })

    it('Should insert user', async (done) => {
        await authentificatedSession
            .post('/admin/create')
            .send({
                name: 'test',
                firstname: 'test',
                email: 'test@test.xyz',
                password: 'TestTest',
                selectRole: 3,
                selectSection: null
            })
            .expect(302)
            .expect('Location', '/admin')
        done()
    })

    // it('Should edit user', async (done) => {
    //     const usrDAO = new UserDAO()
    //     const lastId = usrDAO.create('test', 'test', 'test@test.xyz', 'TestTest', '3', null)

    //     console.log(`User ID : ${lastId}`)
    //     await authentificatedSession
    //         .post(`/admin/editUser/${lastId}/`)
    //         .send({
    //             name: 'test',
    //             firstname: 'test',
    //             email: 'test@test.xyz',
    //             password: 'TestTest',
    //             selectRole: 1,
    //             selectSection: null
    //         })
    //         .expect(302)
    //         .expect('Location', '/admin')
    //     done()
    // })

    it('Should delete user', async (done) => {
        const usrDAO = new UserDAO()
        const User = await usrDAO.find('test@test.xyz')

        await authentificatedSession
            .post(`/admin/delete/${User.id}/`)
            .expect(302)
            .expect('Location', '/admin')
        done()
    })
})