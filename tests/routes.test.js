const request = require('supertest')
const session = require('supertest-session')
const app = require('../app')

describe('Before authentificating session', () => {
    it("Should get /", () => request(app).get("/").expect(200))
    it('Should redirect to /', () => request(app).get("/dashboard").expect(302).expect('Location', '/'))
})

describe('Restricted routes (authorized)', () => {
    let authentificatedSession
    let testSession = null

    beforeEach(() => {
        testSession = session(app)
    })

    beforeEach(done => {
        testSession
            .post("/users/login")
            .send({
                email: 'admin@admin.com',
                password: 'Adminapp'
            })
            .expect(302)
            .expect('Location', '/dashboard')
            .end((err) => {
                if (err) return done(err)
                authentificatedSession = testSession
                return done()
            })
    })

    it('Should get admin page', () => testSession.get('/admin').expect(200))
})

describe('Restricted routes (unauthorized)', () => {
    let authentificatedSession
    let testSession = null

    beforeEach(() => {
        testSession = session(app)
    })

    beforeEach(done => {
        testSession
            .post("/users/login")
            .send({
                email: 'jean@dupont.com',
                password: 'Userapp'
            })
            .expect(302)
            .expect('Location', '/dashboard')
            .end((err) => {
                if (err) return done(err)
                authentificatedSession = testSession
                return done()
            })
    })

    it('Should redirect to dashboard', () => testSession.get('/admin').expect(302).expect('Location', '/dashboard'))
})