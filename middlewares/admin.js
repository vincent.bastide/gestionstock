exports.redirectAdmin = (req, res, next) => {
    if ((req.session.user.id_role > 1) && req.session.user.id_rayon === null) {
        next()
    } else {
        res.redirect('/dashboard')
    }
}