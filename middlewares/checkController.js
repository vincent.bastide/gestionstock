const { body } = require('express-validator')

exports.validateItem = (method) => {
	switch (method) {
		case 'createItem': {
			return [ 
				body('ref', "La référence doit contenir uniquement des chiffres et des lettres"),
				body('name', 'Le nom que vous avez spécifié n’est pas conforme : utilisez des caractères alphabétiques').exists().isLength({ min: 3 }),
				body('quantity', 'Vous n’avez pas entré une quantité valide').exists().isInt(),
				body('price', 'Vous n’avez pas entré un prix valide').exists().isDecimal()
			]   
		}
		case 'editItem': {
			return [ 
				body('name', 'Le nom que vous avez spécifié n’est pas conforme : utilisez des caractères alphabétiques').exists().isLength({ min: 3 }),
				body('quantity', 'Vous n’avez pas entré une quantité valide').exists().isInt(),
				body('price', 'Vous n’avez pas entré un prix valide').exists().isDecimal()
			]   
		}
	}
}
exports.validateUser = (method) => {
	switch (method) {
		case 'createUser': {
			return [ 
				body('name', "Le nom que vous avez spécifié n’est pas conforme : utilisez uniquement des caractères alphabétiques (min : 3 , max : 20)").exists().isAlpha().isLength({ min: 3,max:20 }),
				body('firstname',"Le prénom que vous avez spécifié n’est pas conforme : utilisez uniquement des caractères alphabétiques (min : 2 , max : 20)").exists().isAlpha().isLength({ min: 2,max:20 }),
				body('email', "L'adresse email spécifié ne correspond pas à la forme demandé xxxx@xxx.com/fr").exists().isEmail(),
				body('password',"Le mot de passe doit contenir au moins 6 caractères dont 1 en majuscule").exists().isAlphanumeric().isLength({ min: 6 })
			]   
		}
		case 'editUser': {
			return [ 
				body('name', "Le nom que vous avez spécifié n’est pas conforme : utilisez uniquement des caractères alphabétiques (min : 3 , max : 20)").exists().isAlpha().isLength({ min: 3,max:20 }),
				body('firstname',"Le prénom que vous avez spécifié n’est pas conforme : utilisez uniquement des caractères alphabétiques (min : 3 , max : 20)").exists().isAlpha().isLength({ min: 2,max:20 }),
				body('email', "L'adresse email spécifié ne correspond pas à la forme demandé xxxx@xxx.com/fr").exists().isEmail()
			]   
		}
	}
}