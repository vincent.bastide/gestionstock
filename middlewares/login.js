'use strict'

exports.redirectLogin = (req, res, next) => {
    if (!req.session.user) {
        res.redirect('/')
    } else {
        next()
    }
}

exports.redirectLogged = (req, res, next) => {
    if (req.session.user) {
        res.redirect('/dashboard')
    } else {
        next()
    }
}