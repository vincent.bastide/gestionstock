
describe('Connexion Test', () => {
  it('Connexion : TC01', () => {
    cy.visit('http://localhost:3000/')
	cy.get('input[name=email]').type("admin@admin.com")
    cy.get('input[name=password]').type(`${'admin'}{enter}`)
	cy.url().should('include', '/dashboard')
  })
  it('Connexion : TC02', () => {
    cy.visit('http://localhost:3000/')
	cy.get('input[name=email]').type("guillaume@guillaume.com")
    cy.get('input[name=password]').type(`${'Guillaume'}{enter}`)
	cy.get('div[class="alert alert-danger mt-2"]').should('be.visible')
	cy.url().should('eq', 'http://localhost:3000/?error=ID')
  })
  it('Connexion : TC03', () => {
    cy.visit('http://localhost:3000/')
	//cy.get('input[name=email]').type("admin@admin.com")
    //cy.get('input[name=password]').type(`${''}{enter}`)
	cy.get('button[type=submit]').click()
	cy.url().should('eq', 'http://localhost:3000/')
  })
  it('Connexion : TC04', () => {
    cy.visit('http://localhost:3000/')
	cy.get('input[name=email]').type("admin@admin.com")
   // cy.get('input[name=password]').type(`${'admin'}{enter}`)
	cy.get('button[type=submit]').click()
	cy.url().should('eq', 'http://localhost:3000/')
  })
  it('Connexion : TC05', () => {
    cy.visit('http://localhost:3000/')
	cy.get('input[name=email]').type("adminadmin.com")
    cy.get('input[name=password]').type(`${'admin'}{enter}`)
	cy.url().should('eq', 'http://localhost:3000/')
  })
  it('Connexion : TC06', () => {
    cy.visit('http://localhost:3000/')
	cy.get('input[name=email]').type("admin@admin.com")
    cy.get('input[name=password]').type(`${'admin'}{enter}`)
	cy.url().should('include', '/dashboard')
	cy.get('button[class="btn btn-danger"]').click()
  })
})
