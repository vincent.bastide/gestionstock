#!/bin/bash

echo HOSTDB="mysql" >> .env
echo DBNAME=$MYSQL_DB >> .env
echo USERDB="root" >> .env
echo PORTDB="3306" >> .env
echo PSSWD=$MYSQL_PASS >> .env
echo PORT="3000" >> .env
echo SESS_SECRET=secret >> .env